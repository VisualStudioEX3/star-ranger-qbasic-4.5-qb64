# STAR RANGER
Retro 2D action-platformer experiment written in QBasic 4.5 (using GW-BASIC syntax) for MS-DOS version, and QB64 (https://www.qb64.org/) for Windows, Linux and Mac versions.

Currently abandoned.

![Tests](https://visualstudioex3.com/wp-content/uploads/2017/01/gwbasic8086blttest.jpg)
![GW-BASIC 3.23 and MS-DOS 3.3 manuals and diskets](https://visualstudioex3.com/wp-content/uploads/2017/01/MS-DOS_GW-BASIC_manuals.jpg)